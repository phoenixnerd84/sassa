import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  id = "";
  name = "";
  surname = "";
  contact = "";
  gender = "";
  address = "";
  stage = "id";
  otp = 3412;
  showIdError = false;
  showContactError = false;
  showOTPError = false;
  valid = true;
  constructor() { }

  ngOnInit() {
  }

  submitId(idNumber: any){
    this.showIdError = isNaN(idNumber);
    if(!this.showIdError && idNumber.length === 13){
      this.id = idNumber;
      this.showIdError = false;
      this.stage = "contact";
    } else{
      this.showIdError = true;
    }
  }

  submitContact(contact: any){
    this.showIdError = isNaN(contact);
    if(!this.showIdError && contact.length === 10 ){
      this.contact = contact;
      this.showContactError = false;
      this.stage = "OTP";
    } else{
      this.showIdError = true;
    }
  }

  submitOTP(otp: any){
    console.log(otp);
    if(otp === '3412'){
      this.stage = "nameCheck";
    } else {
      this.showOTPError = true;
    }
  }

  submitName(name1: any, name2: any, name3: any, name4: any, name5: any){
    if(name3 === false){
      this.valid = false;
    }
    this.stage = "bankCheck";
  }

  submitBank(bank1: any, bank2: any){
    if(bank1 === false){
      this.valid = false;
    }
    this.stage = "primaryCheck";

  }

  submitPrimary(primary1: any, primary2: any, primary3: any, primary4: any, primary5: any){
    if( primary5 === false){
      this.valid = false;
    }
    if(this.valid === true){
      this.stage = "bankingDetails";
    } else {
      this.stage = "sorryBaba";
    }
  }

  submitDetails(name: any, surname: any, contact: any, gender: any, address: any){
    this.name = name;
    this.surname = surname;
    this.contact = contact;
    this.gender = gender;
    this.address = address;
  }


}
