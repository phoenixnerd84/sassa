import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfomationPageComponent } from './infomation-page.component';

describe('InfomationPageComponent', () => {
  let component: InfomationPageComponent;
  let fixture: ComponentFixture<InfomationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfomationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfomationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
